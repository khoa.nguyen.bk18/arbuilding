1. get list search api:
	https://poly.google.com/search/{keyWord}

2.
category:
	https://poly.google.com/blocks
	https://poly.google.com/tiltbrush
	https://poly.google.com/category/animals
	https://poly.google.com/category/architecture
	https://poly.google.com/category/art
	https://poly.google.com/category/culture
	https://poly.google.com/category/current-events
	https://poly.google.com/category/food
	https://poly.google.com/category/furniture-home
	https://poly.google.com/category/history
	https://poly.google.com/category/nature
	https://poly.google.com/category/people
	https://poly.google.com/category/places
	https://poly.google.com/category/science
	https://poly.google.com/category/sports-fitness
	https://poly.google.com/category/tech
	https://poly.google.com/category/transport

3. get detail model api
	https://poly.google.com/view/{asset-model-id}

3. download model api:
	https://poly.google.com/view/{asset-model-id}/embed --> get link inside website