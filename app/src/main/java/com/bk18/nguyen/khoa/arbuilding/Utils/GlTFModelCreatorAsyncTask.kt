package com.bk18.nguyen.khoa.arbuilding.Utils

import android.os.AsyncTask
import com.bk18.nguyen.khoa.arbuilding.MainActivity.GltfAssetReaderThreaded
import com.bk18.nguyen.khoa.arbuilding.MainActivity.log
import com.bk18.nguyen.khoa.arbuilding.jGlTF.model.GltfModel
import com.bk18.nguyen.khoa.arbuilding.jGlTF.model.GltfModels
import com.bk18.nguyen.khoa.arbuilding.jGlTF.model.io.IO
import com.bk18.nguyen.khoa.arbuilding.jGlTF.model.io.JsonError
import de.javagl.swing.tasks.ProgressListener
import java.net.URI
import java.text.NumberFormat

class GlTFModelCreatorAsyncTask : AsyncTask<URI, GltfModel, GltfModel>() {

    override fun onPreExecute() {
        super.onPreExecute()
    }

    override fun doInBackground(vararg params: URI?): GltfModel {
        var mUri = params[0]
        var gltfAssetReaderThreaded = GltfAssetReaderThreaded(-1)
        var message = "Loading glTF from ${IO.extractFileName(mUri)}"
        var contentLength = IO.getContentLength(mUri)
        if (contentLength >= 0) {
            var contentLengthString = NumberFormat.getNumberInstance().format(contentLength)
            message += " (${contentLengthString} bytes)"
        }

        var progressListener = object : ProgressListener {
            override fun messageChanged(message: String) {
                log("message: ${message}")
            }

            override fun progressChanged(progress: Double) {
                log("process: ${progress}")
            }
        }

        gltfAssetReaderThreaded.addProgressListener(progressListener)
        gltfAssetReaderThreaded.setJsonErrorConsumer { t: JsonError? ->
            val jsonErrorString = "${t?.message}, at JSON path: ${t?.jsonPathString}"

            log("jsonErrorString: ", jsonErrorString)
        }

        val gltfAsset = gltfAssetReaderThreaded.readGltfAsset(mUri)
        val gltfModel = GltfModels.create(gltfAsset)
        return gltfModel
    }

}