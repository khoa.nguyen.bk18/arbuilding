package com.bk18.nguyen.khoa.arbuilding.activities

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bk18.nguyen.khoa.arbuilding.MainActivity.log
import com.bk18.nguyen.khoa.arbuilding.Models.Model
import com.bk18.nguyen.khoa.arbuilding.R
import com.bk18.nguyen.khoa.arbuilding.Utils.*
import com.bk18.nguyen.khoa.arbuilding.Views.StartSnapHelper
import com.bk18.nguyen.khoa.arbuilding.adapters.HomeModelAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.row_category.view.*
import kotlin.math.max


class HomeActivity : AppCompatActivity() {

    private var mListModels: ArrayList<Model> = ArrayList()
    private lateinit var mAdapterNewModel: HomeModelAdapter
    private var mIsLoadedFirstPage = false
    private var mWebViewListener = object : WebViewClient() {
        override fun onLoadResource(view: WebView?, url: String?) {
            log("url", url.toString())
            url?.let {
                if (url.contains(BASE_URL_PREVIEW_MODEL)) {
                    mListModels.add(Model("", url, null, "", "", "", null))
                }
            }
            super.onLoadResource(view, url)
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            if (!mIsLoadedFirstPage) {
                mListModels.removeAt(0)
                mListModels.removeAt(0)
                extractDataWebView()
                mIsLoadedFirstPage = true
            }
            super.onPageFinished(view, url)
        }

        private fun extractDataWebView() {
            mWebView.evaluateJavascript(EXTRACT_HTML_TO_STRING) { html ->
                var indexID = html.indexOf(DATA_ASSET_ID_KEY)
                var indexIDTitle = html.indexOf(DATA_ASSET_TITLE_KEY)
                var createdTimeAgo = html.indexOf(TIME_CREATED_AGO_KEY)
                var largerIndex = max(indexID, indexIDTitle)

                val matchLength = DATA_ASSET_ID_KEY.length
                var index = 0;
                while (largerIndex >= 0) {

                    val dataAssetId = html.substring(
                        indexID + DATA_ASSET_ID_KEY.length + 2,
                        html.indexOf("\\", indexID + DATA_ASSET_ID_KEY.length + 2)
                    )
                    val dataAssetTitle = html.substring(
                        indexIDTitle + DATA_ASSET_TITLE_KEY.length + 2,
                        html.indexOf("\\", indexIDTitle + DATA_ASSET_TITLE_KEY.length + 2)
                    )

                    val timeCreatedAgo = html.substring(
                        createdTimeAgo + TIME_CREATED_AGO_KEY.length + 3,
                        html.indexOf("\\", createdTimeAgo + TIME_CREATED_AGO_KEY.length + 2)
                    )
                    Log.e("data-asset-id", dataAssetId)
                    Log.e("data-asset-title", dataAssetTitle)
                    Log.e("created: ", timeCreatedAgo)

                    if (index <= mListModels.lastIndex) {
                        mListModels[index].createdTimeAgo = timeCreatedAgo
                        mListModels[index].name = dataAssetTitle
                        mAdapterNewModel.notifyItemChanged(index, HomeModelAdapter.PAYLOAD)
                    }

                    indexID = html.indexOf(DATA_ASSET_ID_KEY, largerIndex + matchLength)
                    indexIDTitle = html.indexOf(DATA_ASSET_TITLE_KEY, largerIndex + matchLength)
                    createdTimeAgo = html.indexOf(TIME_CREATED_AGO_KEY, largerIndex + matchLength)
                    largerIndex = Math.max(indexID, indexIDTitle)
                    index++
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        mListModels.add(Model("", null, null, "", "", "", null))
        mListModels.add(Model("", null, null, "", "", "", null))
        //init recyclerview
        rcvNewModel.layoutManager = LinearLayoutManager(this@HomeActivity, RecyclerView.HORIZONTAL, false)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val layoutParams = rcvNewModel.layoutParams
        layoutParams.height = (displayMetrics.widthPixels * 3) / 6
        rcvNewModel.layoutParams = layoutParams

        mAdapterNewModel = HomeModelAdapter(mListModels)
        rcvNewModel.adapter = mAdapterNewModel
        val snapHelper = StartSnapHelper()
        snapHelper.attachToRecyclerView(rcvNewModel)

        //load webview
        mWebView.loadUrl(BASE_URL)
        mWebView.webViewClient = mWebViewListener

        //init Category Button
        row_category_1.btnCol1.setImageResource(R.drawable.ic_cat_tilt_brush)
        row_category_1.btnCol2.setImageResource(R.drawable.ic_cat_blocks)
        row_category_1.btnCol3.setImageResource(R.drawable.ic_cat_animals)

        row_category_2.btnCol1.setImageResource(R.drawable.ic_cat_architecture)
        row_category_2.btnCol2.setImageResource(R.drawable.ic_cat_art)
        row_category_2.btnCol3.setImageResource(R.drawable.ic_cat_culture)

        row_category_3.btnCol1.setImageResource(R.drawable.ic_cat_food)
        row_category_3.btnCol2.setImageResource(R.drawable.ic_cat_furniture_home)
        row_category_3.btnCol3.setImageResource(R.drawable.ic_cat_history)

        row_category_4.btnCol1.setImageResource(R.drawable.ic_cat_nature)
        row_category_4.btnCol2.setImageResource(R.drawable.ic_cat_people)
        row_category_4.btnCol3.setImageResource(R.drawable.ic_cat_places)

        row_category_5.btnCol1.setImageResource(R.drawable.ic_cat_sport_fitness)
        row_category_5.btnCol2.setImageResource(R.drawable.ic_cat_culture)


        //TODO missing tech transport button
    }

    override fun onResume() {
        super.onResume()
        mWebView.onResume()
    }

    override fun onPause() {
        mWebView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mWebView.onDestroy()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        mWebView.onActivityResult(requestCode, resultCode, intent)
    }


    private fun logLargeString(str: String) {
        if (str.length > 200) {
            if (str.contains("data-asset-id") && str.contains("data-asset-title")) {
                Log.e("modelId:", str.substring(0, 200))
                logLargeString(str.substring(200))
            } else {
                logLargeString(str.substring(200))
            }
        } else {
            if (str.contains("data-asset-id") && str.contains("data-asset-title")) {
                Log.e("modelId:", str)
            }
        }
    }
}