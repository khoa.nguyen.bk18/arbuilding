package com.bk18.nguyen.khoa.arbuilding.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.bk18.nguyen.khoa.arbuilding.MainActivity.log
import com.bk18.nguyen.khoa.arbuilding.Models.Model
import com.bk18.nguyen.khoa.arbuilding.R
import com.bk18.nguyen.khoa.arbuilding.Views.convertDpToPixel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.row_item_model_home.view.*

class HomeModelAdapter(var listModel: ArrayList<Model>) : RecyclerView.Adapter<HomeModelAdapter.ModelViewHolder>() {
    companion object {
        val PAYLOAD = 10
    }

    private val VISIBLE_ITEMS = 1.5
    private lateinit var recyclerView: RecyclerView
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.row_item_model_home, parent, false)
        view.layoutParams.width = ((recyclerView.width / VISIBLE_ITEMS) - convertDpToPixel(20f, parent.context)).toInt()
        log("onCreateViewHolder!")
        return ModelViewHolder(view)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onBindViewHolder(holder: ModelViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isNotEmpty()) {
            when (payloads[0]) {
                PAYLOAD -> {
                    log("PAYLOAD!")
                    holder.mTitle.text = listModel[position].name
                    holder.mCreatedTimeAgo.text = listModel[position].createdTimeAgo
                }
            }
        }
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun getItemCount(): Int {
        return listModel.size
    }

    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
        log("onBindViewHolder!", position.toString())
        val model = listModel[position]
        holder.mTitle.text = model.name
        holder.mCreatedTimeAgo.text = model.createdTimeAgo

        if (model.urlPreview != null) {
            Glide.with(holder.mPreviewModel.context).load(model.urlPreview)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(convertDpToPixel(10f,
                    holder.mPreviewModel.context))))
                .into(holder.mPreviewModel)
            holder.mAniLoading.visibility = View.GONE
        }
    }

    class ModelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mTitle: TextView = itemView.txvTitle
        var mPreviewModel: ImageView = itemView.imgModel
        var mCreatedTimeAgo: TextView = itemView.txvCreatedTimeAgo
        var mAniLoading: LottieAnimationView = itemView.aniLoading

        init {
            log("INIT Model View!")
        }
    }
}