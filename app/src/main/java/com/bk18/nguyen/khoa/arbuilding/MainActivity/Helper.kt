package com.bk18.nguyen.khoa.arbuilding.MainActivity

import android.util.Log

fun log(tag: String = "", msg: String = tag)
{
    val stackTrace: StackTraceElement = Exception().stackTrace.get(1)
    var fileName: String = stackTrace.fileName
    if (fileName == null)
    {
        fileName = ""
    }
    val info: String = stackTrace.methodName + " (" + fileName + ":" + stackTrace.lineNumber + ")"
    Log.e(info, "$tag: $msg")
}

