/**
 * Classes for creating {@link de.javagl.jgltf.model.GltfModel} structures.
 * <br>  
 * These classes are highly preliminary, and should not be considered to 
 * be part of the public API.
 */
package com.bk18.nguyen.khoa.arbuilding.jGlTF.model.impl.creation;

