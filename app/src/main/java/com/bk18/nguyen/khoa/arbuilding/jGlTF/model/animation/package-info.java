/**
 * Classes for handling animations. These are simple (but generic) utility
 * classes for arbitrary animations, largely independent of glTF, but
 * limited to the functionality that is required for glTF.
 */
package com.bk18.nguyen.khoa.arbuilding.jGlTF.model.animation;

