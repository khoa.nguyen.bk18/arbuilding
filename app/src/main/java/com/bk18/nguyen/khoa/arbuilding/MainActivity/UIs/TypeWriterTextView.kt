package com.bk18.nguyen.khoa.arbuilding.MainActivity.UIs

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.widget.EditText

class TypeWriter : EditText {

    private var mText: CharSequence? = null
    private var mIndex: Int = 0
    private var mDelay: Long = 20 // in ms

    private val mHandler = Handler()

    private lateinit var characterAdder: Runnable
    private lateinit var characterDeleter: Runnable

    constructor(context: Context) : super(context) {
        characterDeleter = Runnable {
            hint = mText?.subSequence(0, --mIndex)
            if (mIndex > 0) {
                mHandler.postDelayed(characterDeleter, mDelay)
            } else {
                mOnAnimatedEnd.invoke()
            }
        }

        characterAdder = Runnable {
            hint = mText?.subSequence(0, mIndex++)
            if (mIndex <= mText!!.length) {
                mHandler.postDelayed(characterAdder, mDelay)
            } else {
                mHandler.postDelayed(characterDeleter, mDelay)
            }
        }
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        characterDeleter = Runnable {
            hint = mText?.subSequence(0, --mIndex)
            if (mIndex > 0) {
                mHandler.postDelayed(characterDeleter, mDelay)
            } else {
                mOnAnimatedEnd.invoke()
            }
        }
        characterAdder = Runnable {
            hint = mText?.subSequence(0, mIndex++)
            if (mIndex <= mText!!.length) {
                mHandler.postDelayed(characterAdder, mDelay)
            } else {
                mHandler.postDelayed(characterDeleter, mDelay)
            }
        }
    }

    fun animateText(txt: CharSequence) {
        mText = txt
        mIndex = 0

        hint = ""
        mHandler.removeCallbacks(characterAdder)
        mHandler.postDelayed(characterAdder, mDelay)
    }

    fun setCharacterDelay(m: Long) {
        mDelay = m
    }

    private var mOnAnimatedEnd: () -> Unit = {}
    fun onAnimatedEnd(callBack: () -> Unit) {
        mOnAnimatedEnd = callBack
    }

}