package com.bk18.nguyen.khoa.arbuilding.Models

data class Model(var name: String,
                 var urlPreview: String?,
                 var artist: Artist?,
                 var urlDetail: String,
                 var urlArtist: String,
                 var createdTimeAgo: String,
                 var download: Download?)

data class Artist(var name: String, var urlAvatar: String, var urlLink: String)

data class Download(var urlGlTF: String?, var urlBinary: String?, var urlArchive: String)