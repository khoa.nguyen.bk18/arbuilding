package com.bk18.nguyen.khoa.arbuilding.Utils


import android.os.AsyncTask
import com.bk18.nguyen.khoa.arbuilding.jGlTF.model.GltfConstants
import com.bk18.nguyen.khoa.arbuilding.jGlTF.model.GltfModel
import com.bk18.nguyen.khoa.arbuilding.jGlTF.model.GltfModels
import com.bk18.nguyen.khoa.arbuilding.jGlTF.obj.BufferStrategy
import com.bk18.nguyen.khoa.arbuilding.jGlTF.obj.v2.ObjGltfAssetCreatorV2
import java.net.URI

class ObjModelCreatorAsyncTask : AsyncTask<URI, GltfModel, GltfModel>() {

    private var bufferStrategy = BufferStrategy.BUFFER_PER_FILE
    private var indicesComponentType = GltfConstants.GL_UNSIGNED_SHORT
    private lateinit var mObjGltfAssetCreator: ObjGltfAssetCreatorV2


    override fun onPreExecute() {
        super.onPreExecute()
        mObjGltfAssetCreator = ObjGltfAssetCreatorV2(bufferStrategy)
        mObjGltfAssetCreator.setIndicesComponentType(indicesComponentType)
        mObjGltfAssetCreator.setAssigningRandomColorsToParts(false)
    }

    override fun doInBackground(vararg params: URI?): GltfModel {
        var objURI = params[0]
        val gltfAsset = mObjGltfAssetCreator.create(objURI)
        val gltfModel = GltfModels.create(gltfAsset)
        return gltfModel
    }

}