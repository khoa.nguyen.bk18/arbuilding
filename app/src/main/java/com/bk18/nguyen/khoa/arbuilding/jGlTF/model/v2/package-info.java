/**
 * Implementations for the {@link de.javagl.jgltf.model.GltfModel} classes,
 * based on glTF 2.0.<br>
 * <br>
 * In most cases, these classes should not directly be used by clients.
 */
package com.bk18.nguyen.khoa.arbuilding.jGlTF.model.v2;

