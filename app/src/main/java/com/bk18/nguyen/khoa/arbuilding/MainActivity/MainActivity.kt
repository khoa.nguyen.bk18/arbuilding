package com.bk18.nguyen.khoa.arbuilding.MainActivity

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.bk18.nguyen.khoa.arbuilding.ARActivity
import com.bk18.nguyen.khoa.arbuilding.Decompress
import com.bk18.nguyen.khoa.arbuilding.R
import com.bk18.nguyen.khoa.arbuilding.Utils.GlTFModelCreatorAsyncTask
import com.bk18.nguyen.khoa.arbuilding.Utils.ObjModelCreatorAsyncTask
import com.bk18.nguyen.khoa.arbuilding.jGlTF.model.io.GltfModelWriter
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.Scene
import com.google.ar.sceneform.SkeletonNode
import com.google.ar.sceneform.assets.RenderableSource
import com.google.ar.sceneform.math.Quaternion
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.rendering.ModelRenderable
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.vincent.filepicker.Constant
import com.vincent.filepicker.activity.NormalFilePickActivity
import com.vincent.filepicker.filter.entity.NormalFile
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.net.URI
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    private lateinit var mUri: URI
    private lateinit var UNZIPPED_PATH: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        UNZIPPED_PATH = applicationContext.filesDir.path + "/unzipped"
        btnSelectModel.setOnClickListener {
            selectModelDialog()
        }

        btnOpenARActivity.setOnClickListener {
            //            inflateARActivity()
        }

        var permisisons = ArrayList<String>()
        permisisons.add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
        permisisons.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        permisisons.add(android.Manifest.permission.CAMERA)
        Dexter.withActivity(this@MainActivity)
            .withPermissions(permisisons).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    Log.e("CHECKED", report.toString())
                }

                override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?,
                                                                token: PermissionToken?) {

                }
            })
            .check()


        //TEST PREVIEW MODEL
        scene = scene_view.scene
        searchingForGlbFiles()
    }

    private fun selectModelDialog() {
        var arrayAdapter = arrayOf("Storage", "Link File From Google Drive")
        var dialogLoadFile = AlertDialog.Builder(this)
            .setTitle("Load model file from...")
            .setSingleChoiceItems(arrayAdapter, -1, object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    when (which) {
                        0 -> {
                            loadFileFromStorage()
                        }
                        1 -> {
                            pasteClipboardToEditText()
                        }
                    }
                    dialog?.dismiss()
                }
            })
        var dialog = dialogLoadFile.create()
        dialog.show()

    }

    private fun loadFileFromStorage() {
        val intentLoadFileGLB = Intent(this, NormalFilePickActivity::class.java)
        intentLoadFileGLB.putExtra(Constant.MAX_NUMBER, 1)
        intentLoadFileGLB.putExtra(NormalFilePickActivity.SUFFIX, arrayOf("zip"))
        startActivityForResult(intentLoadFileGLB, Constant.REQUEST_CODE_PICK_FILE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.REQUEST_CODE_PICK_FILE && resultCode == Activity.RESULT_OK) {
            var listFile: ArrayList<NormalFile> = data!!.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE)
            var nameFolder = listFile[0].name
            var internalStorage = applicationContext.filesDir.path + "/unzipped/${nameFolder}/"
            var d = Decompress(listFile[0].path, internalStorage, object : Decompress.UnzipCallBack {
                override fun onUnzipCallback(listOutputPaths: ArrayList<String>) {

                    var modelFile = listOutputPaths.find { path ->
                        path.split('.').last() == "gltf" || path.split('.').last() == "glb" || path.split('.').last() == "obj"
                    }

                    if (modelFile != null) {
                        when (modelFile.split('.').last()) {
                            "glb" -> {
                                processingGlbFile(modelFile)
                            }
                            "gltf" -> {
                                processingGltfFiles(internalStorage, modelFile)
                            }
                            "obj" -> {
                                processingObjFiles(internalStorage, modelFile)
                            }
                        }
                    }
                }
            })
            d.unzip()
        }
    }

    private fun processingObjFiles(internalStorage: String, modelFile: String) {
        //Processing obj file
        var objFile = File(modelFile)

        var gltfModel = ObjModelCreatorAsyncTask().execute(objFile.toURI()).get()
        var outputFile = File(internalStorage + "/${objFile.nameWithoutExtension}_output.glb")
        var outputStream = FileOutputStream(outputFile)
        var gltfModelWriter = GltfModelWriter()
        gltfModelWriter.writeBinary(gltfModel, outputStream)
        var listUris = ArrayList<Uri>()
        listUris.add(Uri.fromFile(outputFile))
        openArActivity(listUris)
    }

    private fun processingGlbFile(modelFile: String) {
        var gltfFile = File(modelFile)
        var listUris = ArrayList<Uri>()
        listUris.add(Uri.fromFile(gltfFile))
        openArActivity(listUris)
    }

    private fun processingGltfFiles(internalStorage: String, modelFile: String) {
        var gltfFile = File(modelFile)
        mUri = gltfFile.toURI()
        var outputFile = File(internalStorage + "/${gltfFile.nameWithoutExtension}_output.glb")
        var outputStream = FileOutputStream(outputFile)
        var gltfModel = GlTFModelCreatorAsyncTask().execute(mUri).get()
        var gltfModelWriter = GltfModelWriter()
        gltfModelWriter.writeBinary(gltfModel, outputStream)
        var listUris = ArrayList<Uri>()
        listUris.add(Uri.fromFile(outputFile))
        openArActivity(listUris)
    }

    private fun searchingForGlbFiles() {
        val unzippedFolder = File(UNZIPPED_PATH).list()
        var listModels = ArrayList<File>()
        unzippedFolder?.let {
            if (unzippedFolder.isNotEmpty()) {
                for (folder in unzippedFolder) {
                    val modelFile =
                        File(UNZIPPED_PATH + "/" + folder + "/").listFiles()
                    for (file in modelFile) {
                        if (file.extension == "glb") {
                            listModels.add(file)
                            break
                        }
                    }
                }
            }
        }

        if (listModels.isNotEmpty()) {
            for (file in listModels) {
                log("model file:", file.path)
                val tvModel = Button(this)
                tvModel.text = file.name
                main_activity.addView(tvModel)
                tvModel.setOnClickListener {
                    val uris = ArrayList<Uri>()
                    uris.add(Uri.fromFile(file))
                    openArActivity(uris)
                }
            }
            this@MainActivity.runOnUiThread {
                renderObject(listModels[0])
            }

        }
    }

    private fun openArActivity(listUri: ArrayList<Uri>) {
        var intent = Intent(this@MainActivity, ARActivity::class.java)
        intent.putExtra("uri", listUri)
        startActivity(intent)
    }

    private fun pasteClipboardToEditText() {
        var url = "https://drive.google.com/uc?export=download&id=1MngtXxQeoEhOk-aezPoqaW49Za5YQG5s"
        PRDownloader.initialize(applicationContext)
        var internalStorage = applicationContext.filesDir.path + "/download/"
        var fileName = "${Date().time}_download.zip"
        PRDownloader.download(url, internalStorage, fileName).build().start(object : OnDownloadListener {
            override fun onDownloadComplete() {
                var downloadedFile = File(internalStorage + fileName)
                var d = Decompress(downloadedFile.path, internalStorage, object : Decompress.UnzipCallBack {
                    override fun onUnzipCallback(listOutputPaths: ArrayList<String>) {
                        var modelFile = listOutputPaths.find { path ->
                            path.split('.').last() == "gltf" || path.split('.').last() == "glb" || path.split('.').last() == "obj"
                        }

                        if (modelFile != null) {
                            when (modelFile.split('.').last()) {
                                "glb" -> {
                                    processingGlbFile(modelFile)
                                }
                                "gltf" -> {
                                    processingGltfFiles(internalStorage, modelFile)
                                }
                                "obj" -> {
                                    processingObjFiles(internalStorage, modelFile)
                                }
                            }
                        }
                    }
                })
                d.unzip()
            }

            override fun onError(error: Error?) {
            }
        })


//        var clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
//        var clipData = clipboardManager.primaryClip
//        var itemClip = clipData?.getItemAt(0)
//        var url = itemClip?.text.toString()
//        if (url.isBlank() || url.isEmpty()) {
//            Toast.makeText(this, "Copy your file url to paste", Toast.LENGTH_SHORT).show()
//        } else {
//
//        }
    }

    //RENDER PREVIEW 3D MODEL

    private fun renderObject(parse: File) {

        val renderableSource =
            RenderableSource.builder()
                .setSource(this, Uri.fromFile(parse), RenderableSource.SourceType.GLB)
                .setScale(0.1F)
                .build()

        ModelRenderable.builder()
            .setSource(this, renderableSource)
            .setRegistryId(Uri.fromFile(parse))
            .build()
            .thenAccept {
                log("model", it.toString())
                addNodeToScene(it)
            }
            .exceptionally {
                val builder = AlertDialog.Builder(this)
                builder.setMessage(it.message)
                    .setTitle("error!")
                val dialog = builder.create()
                dialog.show()
                return@exceptionally null
            }

    }

    /**
     * Adds a node to the current scene
     * @param model - rendered model
     */
    lateinit var scene: Scene

    lateinit var cupCakeNode: Node
    private lateinit var andy: SkeletonNode
    private var valueLeft = 10.0f
    private var valueRight = 10.0f
    private lateinit var mRenderable: ModelRenderable
    private fun addNodeToScene(model: ModelRenderable?) {
        model?.let {
            cupCakeNode = Node().apply {
                setParent(scene)
                localPosition = Vector3(0f, 0f, -1f)
                localScale = Vector3(3f, 3f, 3f)

                name = "Cupcake"
                renderable = it
                mRenderable = it
            }

            scene.addChild(cupCakeNode)
            cupCakeNode.localRotation = Quaternion.axisAngle(cupCakeNode.localPosition, 180f)
        }

        btnLeft.setOnClickListener {
            valueLeft += 50
            cupCakeNode.apply {
                localRotation = Quaternion.axisAngle(Vector3(0.0f, 1.0f, 0.0f), valueLeft)
            }
        }
        btnRight.setOnClickListener {
            valueRight += 50
            cupCakeNode.apply {
                localRotation = Quaternion.axisAngle(Vector3(0.0f, 1.0f, 0.0f), -valueRight)
            }
        }

        //TEST Rotate model
        btnRotate.setOnClickListener {
            val x = edt_vec_x.text.toString().toFloatOrNull() ?: cupCakeNode.localPosition.x
            val y = edt_vec_y.text.toString().toFloatOrNull() ?: cupCakeNode.localPosition.y
            val z = edt_vec_z.text.toString().toFloatOrNull() ?: cupCakeNode.localPosition.z
            var rotateDeg = edt_rotate.text.toString().toFloatOrNull() ?: 180f
            cupCakeNode.localRotation = Quaternion.axisAngle(Vector3(x, y, z), rotateDeg)
        }


        //animation data test
        val aniData = mRenderable.animationDataCount
        Log.e("aniData", aniData.toString())

        andy = SkeletonNode()
        andy.setParent(cupCakeNode)
        andy.renderable = mRenderable
    }

    override fun onPause() {
        super.onPause()
        scene_view.pause()
    }

    override fun onResume() {
        super.onResume()
        scene_view.resume()
    }
}