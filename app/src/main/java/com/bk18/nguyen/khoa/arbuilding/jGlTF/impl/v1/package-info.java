/**
 * Implementation of the glTF structure. The classes in this package
 * are automatically generated from the schema. They may change when
 * the schema is updated, and should not be modified manually.
 */
package com.bk18.nguyen.khoa.arbuilding.jGlTF.impl.v1;

