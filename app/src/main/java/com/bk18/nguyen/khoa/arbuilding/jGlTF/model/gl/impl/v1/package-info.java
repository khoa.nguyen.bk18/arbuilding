/**
 * Implementations of the <code>de.javagl.jgltf.model.gl</code> classes,
 * for glTF 1.0.<br>
 * <br>  
 * These classes should not be considered to be part of the public API.
 */
package com.bk18.nguyen.khoa.arbuilding.jGlTF.model.gl.impl.v1;

