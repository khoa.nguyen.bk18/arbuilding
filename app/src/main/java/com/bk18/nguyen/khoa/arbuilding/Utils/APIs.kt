package com.bk18.nguyen.khoa.arbuilding.Utils

val BASE_URL = "https://poly.google.com"
val BASE_URL_PREVIEW_MODEL = "https://lh3.googleusercontent.com"

val URL_TILT_BRUSH = "${BASE_URL}/tiltbrush"
val URL_BLOCKS = "${BASE_URL}/blocks"

val CATEGORY = "category"
val URL_CAT_ANIMALS = "${BASE_URL}/${CATEGORY}/animals"
val URL_CAT_ARCHITECTURE = "${BASE_URL}/${CATEGORY}/architecture"
val URL_CAT_ART = "${BASE_URL}/${CATEGORY}/art"
val URL_CAT_CULTURE = "${BASE_URL}/${CATEGORY}/culture"
val URL_CAT_CURRENT_EVENTS = "${BASE_URL}/${CATEGORY}/current-events"
val URL_CAT_FOOD = "${BASE_URL}/${CATEGORY}/food"
val URL_CAT_FURNITURE_HOME = "${BASE_URL}/${CATEGORY}/furniture-home"
val URL_CAT_HISTORY = "${BASE_URL}/${CATEGORY}/history"
val URL_CAT_NATURE = "${BASE_URL}/${CATEGORY}/nature"
val URL_CAT_PEOPLE = "${BASE_URL}/${CATEGORY}/people"
val URL_CAT_PLACES = "${BASE_URL}/${CATEGORY}/places"
val URL_CAT_SCIENCE = "${BASE_URL}/${CATEGORY}/science"
val URL_CAT_SPORTS_FITNESS = "${BASE_URL}/${CATEGORY}/sport-fitness"
val URL_CAT_TECH = "${BASE_URL}/${CATEGORY}/tech"
val URL_CAT_TRANSPORT = "${BASE_URL}/${CATEGORY}/transport"