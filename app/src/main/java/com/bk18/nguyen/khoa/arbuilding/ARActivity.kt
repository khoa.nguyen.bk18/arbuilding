package com.bk18.nguyen.khoa.arbuilding

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bk18.nguyen.khoa.arbuilding.MainActivity.log
import com.google.ar.core.Anchor
import com.google.ar.core.HitResult
import com.google.ar.core.Plane
import com.google.ar.core.TrackingState
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.assets.RenderableSource
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import kotlinx.android.synthetic.main.activity_ar.*


class ARActivity : AppCompatActivity() {
    private lateinit var arFragment: ArFragment

    private var isTracking: Boolean = false
    private var isHitting: Boolean = false
    private var mListModelRenderable = ArrayList<ModelRenderable>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ar)

        var listModel = ArrayList<Uri>()
        if (intent.extras != null) {
            listModel = intent.getParcelableArrayListExtra("uri")
        }


        buildObjects(listModel)

        var index = 0
        fab.setOnClickListener {
            addObject(listModel[index])
            index++
            if (index >= listModel.size - 1) {
                index = 0
                Toast.makeText(this, "Model is out of", Toast.LENGTH_SHORT).show()
            }

        }
        showFab(false)
    }


    private fun buildObjects(listUri: ArrayList<Uri>) {
        var unitIncrement: Double = (100.0 / listUri.size.toDouble())
        Log.e("unitIncrement:", "$unitIncrement")
        var dialog = setProgressDialog()
        for (i in 0 until listUri.size) {
            val renderableSource = RenderableSource.builder()
                .setSource(
                    this,
                    listUri[i],
                    RenderableSource.SourceType.GLB
                )
//                .setScale(0.001f)
                .build()

            //Most Heavy stuff

            log("registyID: ${listUri[i]}")
            ModelRenderable.builder()
                .setSource(this, renderableSource)
                .setRegistryId(listUri[i])
                .build()
                .thenAccept {
                    mListModelRenderable.add(it)
                    Log.e("before:", "${dialog.second.progress}")
                    dialog.second.progress = (dialog.second.progress + unitIncrement).toInt()
                    Log.e("after:", "${dialog.second.progress}")
                    if (i == listUri.size - 1) {
                        Log.e("load done", "${listUri.size} models")
                        dialog.first?.dismiss()
                        setupARFragment()
                        initRecyclerView()
                    }
                }
                .exceptionally {
                    Toast.makeText(this@ARActivity, "Could not fetch model from ${listUri[i].path}", Toast.LENGTH_SHORT)
                        .show()
                    return@exceptionally null
                }
        }
    }

    private fun setupARFragment() {
        arFragment = sceneform_fragment as ArFragment
        arFragment.arSceneView.scene.addOnUpdateListener()
        { frameTime ->
            arFragment.onUpdate(frameTime)
            onUpdate()
        }
    }

    private fun initRecyclerView() {
    }

    private fun setProgressDialog(): Pair<AlertDialog?, ProgressBar> {
        val llPadding = 30
        val ll = LinearLayout(this)
        ll.orientation = LinearLayout.VERTICAL
        ll.setPadding(llPadding, llPadding, llPadding, llPadding)
        ll.gravity = Gravity.CENTER
        var llParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam

        val progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal)
        progressBar.max = 100
        progressBar.setPadding(0, 0, llPadding, 0)
        progressBar.layoutParams = llParam

        llParam = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(this)
        tvText.text = "Loading ..."
        tvText.setTextColor(Color.parseColor("#000000"))
        tvText.textSize = 20f
        tvText.layoutParams = llParam

        ll.addView(progressBar)
        ll.addView(tvText)

        var handler = Handler()
        var runnable = object : Runnable {
            var count = 0
            override fun run() {
                count++;

                if (count == 1) {
                    tvText.text = "Loading."
                } else if (count == 2) {
                    tvText.text = "Loading.."
                } else if (count == 3) {
                    tvText.text = "Loading..."
                }

                if (count == 3)
                    count = 0

                handler.postDelayed(this, 500)
            }
        }

        handler.postDelayed(runnable, 1000)

        val builder = AlertDialog.Builder(this)
        builder.setCancelable(true)
        builder.setView(ll)

        val dialog = builder.create()
        dialog.show()
        val window = dialog.window
        if (window != null) {
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window.attributes)
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog.window.attributes = layoutParams
        }
        return Pair(dialog, progressBar)
    }


    @SuppressLint("RestrictedApi")
    private fun showFab(enabled: Boolean) {
        if (enabled) {
            fab.isEnabled = true
            fab.visibility = View.VISIBLE
        } else {
            fab.isEnabled = false
            fab.visibility = View.GONE
        }
    }

    // Updates the tracking state
    private fun onUpdate() {
        updateTracking()
        // Check if the devices gaze is hitting a plane detected by ARCore
        if (isTracking) {
            val hitTestChanged = updateHitTest()
            if (hitTestChanged) {
                showFab(isHitting)
            }
        }

    }

    // Performs frame.HitTest and returns if a hit is detected
    private fun updateHitTest(): Boolean {
        val frame = arFragment.arSceneView.arFrame
        val point = getScreenCenter()
        val hits: List<HitResult>
        val wasHitting = isHitting
        isHitting = false
        if (frame != null) {
            hits = frame.hitTest(point.x.toFloat(), point.y.toFloat())
            for (hit in hits) {
                val trackable = hit.trackable
                if (trackable is Plane && trackable.isPoseInPolygon(hit.hitPose)) {
                    isHitting = true
                    break
                }
            }
        }
        return wasHitting != isHitting
    }

    // Makes use of ARCore's camera state and returns true if the tracking state has changed
    private fun updateTracking(): Boolean {
        val frame = arFragment.arSceneView.arFrame
        val wasTracking = isTracking
        isTracking = frame?.camera?.trackingState == TrackingState.TRACKING
        return isTracking != wasTracking
    }

    // Simply returns the center of the screen
    private fun getScreenCenter(): Point {
        val view = findViewById<View>(android.R.id.content)
        return Point(view.width / 2, view.height / 2)
    }

    /**
     * @param model The Uri of our 3D sfb file
     *
     * This method takes in our 3D model and performs a hit test to determine where to place it
     */
    private fun addObject(model: Uri) {
        val frame = arFragment.arSceneView.arFrame
        val point = getScreenCenter()
        if (frame != null) {
            val hits = frame.hitTest(point.x.toFloat(), point.y.toFloat())
            for (hit in hits) {
                val trackable = hit.trackable
                if (trackable is Plane && trackable.isPoseInPolygon(hit.hitPose)) {
                    placeObject(arFragment, hit.createAnchor(), model)
                    break
                }
            }
        }
    }

    private fun addObject(model: ModelRenderable?) {
        val frame = arFragment.arSceneView.arFrame
        val point = getScreenCenter()
        if (frame != null) {
            val hits = frame.hitTest(point.x.toFloat(), point.y.toFloat())
            for (hit in hits) {
                val trackable = hit.trackable
                if (trackable is Plane && trackable.isPoseInPolygon(hit.hitPose)) {
                    if (model != null) {
                        addNodeToScene(arFragment, hit.createAnchor(), model)
                        break
                    }

                }
            }
        }
    }

    /**
     * @param fragment our fragment
     * @param anchor ARCore anchor from the hit test
     * @param model our 3D model of choice
     *
     * Uses the ARCore anchor from the hitTest result and builds the Sceneform nodes.
     * It starts the asynchronous loading of the 3D model using the ModelRenderable builder.
     */
    private fun placeObject(fragment: ArFragment, anchor: Anchor, model: Uri) {
        val renderableSource = RenderableSource.builder()
            .setSource(
                fragment.context,
                model,
                RenderableSource.SourceType.GLB
            )
            .setScale(0.001f)
            .build()

        //Most Heavy stuff
        ModelRenderable.builder()
            .setSource(fragment.context, renderableSource)
            .setRegistryId(model)
            .build()
            .thenAccept {
                addNodeToScene(fragment, anchor, it)
            }
            .exceptionally {
                Toast.makeText(this@ARActivity, "Could not fetch model from $model", Toast.LENGTH_SHORT).show()
                return@exceptionally null
            }
    }

    /**
     * @param fragment our fragment
     * @param anchor ARCore anchor
     * @param renderable our model created as a Sceneform Renderable
     *
     * This method builds two nodes and attaches them to our scene_o
     * The Anchor nodes is positioned based on the pose of an ARCore Anchor. They stay positioned in the sample place relative to the real world.
     * The Transformable node is our Model
     * Once the nodes are connected we select the TransformableNode so it is available for interactions
     */
    private fun addNodeToScene(fragment: ArFragment, anchor: Anchor, renderable: ModelRenderable) {
        val anchorNode = AnchorNode(anchor)
        // TransformableNode means the user to move, scale and rotate the model
        val transformableNode = TransformableNode(fragment.transformationSystem)
        transformableNode.renderable = renderable
        transformableNode.setParent(anchorNode)
        fragment.arSceneView.scene.addChild(anchorNode)
        transformableNode.select()


        //set Animation of object
        val animationData = renderable.animationDataCount
        Log.e("shiba inu ani", animationData.toString())
    }
}
