package com.bk18.nguyen.khoa.arbuilding;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Decompress {
    private String _zipFile;
    private String _location;
    private UnzipCallBack _unZipCallback;

    public Decompress(String zipFile, String location, UnzipCallBack unZipCallback) {
        _zipFile = zipFile;
        _location = location;

        _dirChecker("");
        _unZipCallback = unZipCallback;
    }

    public void unzip() {
        Thread unzipThread = new Thread(() -> {
            try {
                ArrayList<String> outputFilePaths = new ArrayList<>();
                FileInputStream fin = new FileInputStream(_zipFile);
                ZipInputStream zin = new ZipInputStream(fin);
                ZipEntry ze = null;
                while ((ze = zin.getNextEntry()) != null) {
                    Log.v("Decompress", "Unzipping " + ze.getName());

                    if (ze.isDirectory()) {
                        _dirChecker(ze.getName());
                    } else {
                        FileOutputStream fout = new FileOutputStream(_location + ze.getName());
                        BufferedOutputStream bufout = new BufferedOutputStream(fout);
                        byte[] buffer = new byte[1024];
                        int read = 0;
                        while ((read = zin.read(buffer)) != -1) {
                            bufout.write(buffer, 0, read);
                        }
                        outputFilePaths.add(_location + ze.getName());
                        zin.closeEntry();
                        bufout.close();
                        fout.close();
                    }

                }
                zin.close();
                _unZipCallback.onUnzipCallback(outputFilePaths);
            } catch (Exception e) {
                Log.e("Decompress", "unzip", e);
            } finally {

            }
        });

        unzipThread.start();
    }

    private void _dirChecker(String dir) {
        File f = new File(_location + dir);

        if (!f.isDirectory()) {
            boolean mkdir = f.mkdirs();
            if (!mkdir) {
                Log.e("create folder", "FAILED");
            }
        }
    }

    public interface UnzipCallBack {
        void onUnzipCallback(ArrayList<String> listOutputPaths);
    }
}
